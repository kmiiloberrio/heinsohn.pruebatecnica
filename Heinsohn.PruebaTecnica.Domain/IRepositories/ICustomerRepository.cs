﻿using Heinsohn.PruebaTecnica.Domain.Entities;

namespace Heinsohn.PruebaTecnica.Domain.IRepositories
{
    public interface ICustomerRepository : IGenericContextRepository<Customer>
    {
        Customer GetById(string id);
    }
}