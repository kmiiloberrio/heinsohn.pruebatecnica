﻿using System;
using System.Collections.Generic;

namespace Heinsohn.PruebaTecnica.Domain.IRepositories
{
    public interface IGenericInMemRepository<T> where T : class
    {
        T GetById(Func<T, bool> predicate);
        IEnumerable<T> GetAll();
        IEnumerable<T> Find(Func<T, bool> predicate);
        void Add(T entity);
        void AddRange(IEnumerable<T> entities);
        void Update(Predicate<T> predicate, T entity);
        void Remove(Predicate<T> predicate);
        void RemoveRange(IEnumerable<T> entities);
    }
}