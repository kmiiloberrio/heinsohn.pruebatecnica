﻿using Heinsohn.PruebaTecnica.Domain.Entities;

namespace Heinsohn.PruebaTecnica.Domain.IRepositories
{
    public interface IOrderRepository : IGenericContextRepository<Order>
    {
    }
}