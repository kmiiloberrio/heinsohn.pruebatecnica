﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Heinsohn.PruebaTecnica.Domain.IRepositories
{
    public interface IGenericContextRepository<T> where T : class
    {
        T GetById(int id);
        IEnumerable<T> GetAll();
        IEnumerable<T> Find(Expression<Func<T, bool>> expression);

        IEnumerable<T> FindAll(params Expression<Func<T, object>>[]
            includes);

        void Add(T entity);
        void AddRange(IEnumerable<T> entities);
        void Update(T entity);
        void Remove(T entity);
        void RemoveRange(IEnumerable<T> entities);
    }
}