﻿using Heinsohn.PruebaTecnica.Domain.Entities;

namespace Heinsohn.PruebaTecnica.Domain.IRepositories
{
    public interface IOrderDetailRepository : IGenericContextRepository<OrderDetail>
    {
        OrderDetail GetById(int orderId, int productId);
    }
}