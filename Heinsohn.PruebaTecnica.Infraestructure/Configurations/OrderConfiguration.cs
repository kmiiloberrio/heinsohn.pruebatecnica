﻿using Heinsohn.PruebaTecnica.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Heinsohn.PruebaTecnica.Infraestructure.Configurations
{
    internal class OrderConfiguration : SoftDeletedConfiguration<Order>
    {
        public override void Configure(EntityTypeBuilder<Order> builder)
        {
            builder.Property(e => e.OrderId).HasColumnName("OrderID");

            builder.Property(e => e.CustomerId)
                .HasColumnName("CustomerID")
                .HasMaxLength(5);

            builder.Property(e => e.OrderDate).HasColumnType("datetime");

            builder.Property(e => e.RequiredDate).HasColumnType("datetime");
            base.Configure(builder);
        }
    }
}