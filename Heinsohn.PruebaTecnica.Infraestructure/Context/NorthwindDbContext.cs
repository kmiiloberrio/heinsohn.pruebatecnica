﻿using Heinsohn.PruebaTecnica.Domain.Entities;
using Heinsohn.PruebaTecnica.Infraestructure.Context.Extensions;
using Microsoft.EntityFrameworkCore;

namespace Heinsohn.PruebaTecnica.Infraestructure.Context
{
    public class NorthwindDbContext : DbContext
    {
        public NorthwindDbContext(DbContextOptions<NorthwindDbContext> options) : base(options)
        {
        }

        private DbSet<Customer> Customers { get; set; }

        private DbSet<Order> Orders { get; set; }

        private DbSet<OrderDetail> OrderDetails { get; set; }

        private DbSet<Category> Categories { get; set; }

        private DbSet<Product> Products { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(NorthwindDbContext).Assembly);
            modelBuilder.Seed();
            base.OnModelCreating(modelBuilder);
        }

        public override int SaveChanges()
        {
            UpdateSoftDeleteStatuses();
            return base.SaveChanges();
        }

        private void UpdateSoftDeleteStatuses()
        {
            foreach (var entityEntry in ChangeTracker.Entries())
                switch (entityEntry.State)
                {
                    case EntityState.Added:
                        entityEntry.CurrentValues["IsDeleted"] = false;
                        break;
                    case EntityState.Deleted:
                        entityEntry.State = EntityState.Modified;
                        entityEntry.CurrentValues["IsDeleted"] = true;
                        break;
                }
        }
    }
}