﻿using Heinsohn.PruebaTecnica.Application.IServices;
using Heinsohn.PruebaTecnica.Application.Services;
using Heinsohn.PruebaTecnica.Domain.IRepositories;
using Heinsohn.PruebaTecnica.Infraestructure.Repositories;
using Microsoft.Extensions.DependencyInjection;

namespace Heinsohn.PruebaTecnica.Infraestructure
{
    public class DependencyInjection
    {
        public static void RegisterApplicationServices(IServiceCollection services)
        {
            // Heinsohn.PruebaTecnica.Application
            services.AddSingleton<IItemService, ItemService>();
            services.AddTransient<ICustomerService, CustomerService>();
            services.AddTransient<IProductService, ProductService>();
            services.AddTransient<IOrderService, OrderService>();
            services.AddTransient<IOrderDetailService, OrderDetailService>();
            services.AddTransient<ICategoryService, CategoryService>();
        }

        public static void RegisterRepository(IServiceCollection services)
        {
            //CleanArchitecture.Domain.Interfaces | CleanArchitecture.Infra.Data.Repositories
            services.AddScoped(typeof(IGenericInMemRepository<>), typeof(GenericInMemRepository<>));
            services.AddScoped(typeof(IGenericContextRepository<>), typeof(GenericContextRepository<>));
            services.AddTransient<IInMemItemsRepository, InMemItemsRepository>();
            services.AddTransient<ICategoryRepository, CategoryRepository>();
            services.AddTransient<IProductRepository, ProductRepository>();
            services.AddTransient<IOrderRepository, OrderRepository>();
            services.AddTransient<IOrderDetailRepository, OrderDetailRepository>();
            services.AddTransient<ICustomerRepository, CustomerRepository>();
        }
    }
}