﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Heinsohn.PruebaTecnica.Infraestructure.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                "Categories",
                table => new
                {
                    CategoryID = table.Column<int>("int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CategoryName = table.Column<string>("nvarchar(15)", maxLength: 15, nullable: false),
                    Description = table.Column<string>("ntext", nullable: true)
                },
                constraints: table => { table.PrimaryKey("PK_Categories", x => x.CategoryID); });

            migrationBuilder.CreateTable(
                "Customers",
                table => new
                {
                    CustomerID = table.Column<string>("nvarchar(5)", maxLength: 5, nullable: false),
                    CompanyName = table.Column<string>("nvarchar(40)", maxLength: 40, nullable: false),
                    ContactName = table.Column<string>("nvarchar(30)", maxLength: 30, nullable: true),
                    ContactTitle = table.Column<string>("nvarchar(30)", maxLength: 30, nullable: true),
                    Address = table.Column<string>("nvarchar(60)", maxLength: 60, nullable: true),
                    City = table.Column<string>("nvarchar(15)", maxLength: 15, nullable: true),
                    Region = table.Column<string>("nvarchar(15)", maxLength: 15, nullable: true),
                    PostalCode = table.Column<string>("nvarchar(10)", maxLength: 10, nullable: true),
                    Country = table.Column<string>("nvarchar(15)", maxLength: 15, nullable: true),
                    Phone = table.Column<string>("nvarchar(24)", maxLength: 24, nullable: true),
                    Fax = table.Column<string>("nvarchar(24)", maxLength: 24, nullable: true)
                },
                constraints: table => { table.PrimaryKey("PK_Customers", x => x.CustomerID); });

            migrationBuilder.CreateTable(
                "Products",
                table => new
                {
                    ProductID = table.Column<int>("int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProductName = table.Column<string>("nvarchar(40)", maxLength: 40, nullable: false),
                    CategoryID = table.Column<int>("int", nullable: true),
                    QuantityPerUnit = table.Column<string>("nvarchar(20)", maxLength: 20, nullable: true),
                    UnitPrice = table.Column<decimal>("money", nullable: true, defaultValueSql: "((0))"),
                    UnitsInStock = table.Column<short>("smallint", nullable: true, defaultValueSql: "((0))"),
                    UnitsOnOrder = table.Column<short>("smallint", nullable: true, defaultValueSql: "((0))"),
                    ReorderLevel = table.Column<short>("smallint", nullable: true, defaultValueSql: "((0))"),
                    Discontinued = table.Column<bool>("bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Products", x => x.ProductID);
                    table.ForeignKey(
                        "FK_Products_Categories_CategoryID",
                        x => x.CategoryID,
                        "Categories",
                        "CategoryID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                "Orders",
                table => new
                {
                    OrderID = table.Column<int>("int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CustomerID = table.Column<string>("nvarchar(5)", maxLength: 5, nullable: true),
                    OrderDate = table.Column<DateTime>("datetime", nullable: true),
                    RequiredDate = table.Column<DateTime>("datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Orders", x => x.OrderID);
                    table.ForeignKey(
                        "FK_Orders_Customers_CustomerID",
                        x => x.CustomerID,
                        "Customers",
                        "CustomerID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                "Order Details",
                table => new
                {
                    OrderID = table.Column<int>("int", nullable: false),
                    ProductID = table.Column<int>("int", nullable: false),
                    UnitPrice = table.Column<decimal>("money", nullable: false),
                    Quantity = table.Column<short>("smallint", nullable: false, defaultValueSql: "((1))")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Order Details", x => new {x.OrderID, x.ProductID});
                    table.ForeignKey(
                        "FK_Order_Details_Orders",
                        x => x.OrderID,
                        "Orders",
                        "OrderID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        "FK_Order_Details_Products",
                        x => x.ProductID,
                        "Products",
                        "ProductID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                "IX_Order Details_ProductID",
                "Order Details",
                "ProductID");

            migrationBuilder.CreateIndex(
                "IX_Orders_CustomerID",
                "Orders",
                "CustomerID");

            migrationBuilder.CreateIndex(
                "IX_Products_CategoryID",
                "Products",
                "CategoryID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                "Order Details");

            migrationBuilder.DropTable(
                "Orders");

            migrationBuilder.DropTable(
                "Products");

            migrationBuilder.DropTable(
                "Customers");

            migrationBuilder.DropTable(
                "Categories");
        }
    }
}