﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Heinsohn.PruebaTecnica.Infraestructure.Migrations
{
    public partial class InitializeDataOrderDetails : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                "Order Details",
                new[] {"OrderID", "ProductID", "Quantity", "UnitPrice"},
                new object[,]
                {
                    {1, 1, (short) 20, 15.20m},
                    {1, 15, (short) 35, 13.90m},
                    {3, 15, (short) 25, 35.10m},
                    {3, 9, (short) 6, 14.40m},
                    {3, 7, (short) 15, 10.40m},
                    {4, 2, (short) 50, 15.20m},
                    {4, 5, (short) 65, 17.00m},
                    {4, 7, (short) 6, 25.60m},
                    {2, 13, (short) 15, 26.20m},
                    {2, 2, (short) 12, 10.40m}
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                "Order Details",
                new[] {"OrderID", "ProductID"},
                new object[] {1, 1});

            migrationBuilder.DeleteData(
                "Order Details",
                new[] {"OrderID", "ProductID"},
                new object[] {1, 15});

            migrationBuilder.DeleteData(
                "Order Details",
                new[] {"OrderID", "ProductID"},
                new object[] {2, 2});

            migrationBuilder.DeleteData(
                "Order Details",
                new[] {"OrderID", "ProductID"},
                new object[] {2, 13});

            migrationBuilder.DeleteData(
                "Order Details",
                new[] {"OrderID", "ProductID"},
                new object[] {3, 7});

            migrationBuilder.DeleteData(
                "Order Details",
                new[] {"OrderID", "ProductID"},
                new object[] {3, 9});

            migrationBuilder.DeleteData(
                "Order Details",
                new[] {"OrderID", "ProductID"},
                new object[] {3, 15});

            migrationBuilder.DeleteData(
                "Order Details",
                new[] {"OrderID", "ProductID"},
                new object[] {4, 2});

            migrationBuilder.DeleteData(
                "Order Details",
                new[] {"OrderID", "ProductID"},
                new object[] {4, 5});

            migrationBuilder.DeleteData(
                "Order Details",
                new[] {"OrderID", "ProductID"},
                new object[] {4, 7});
        }
    }
}