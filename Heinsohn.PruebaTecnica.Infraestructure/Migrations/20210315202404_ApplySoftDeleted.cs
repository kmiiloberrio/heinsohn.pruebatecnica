﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Heinsohn.PruebaTecnica.Infraestructure.Migrations
{
    public partial class ApplySoftDeleted : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                "IsDeleted",
                "Products",
                "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                "IsDeleted",
                "Orders",
                "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                "IsDeleted",
                "Order Details",
                "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                "IsDeleted",
                "Customers",
                "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                "IsDeleted",
                "Categories",
                "bit",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                "IsDeleted",
                "Products");

            migrationBuilder.DropColumn(
                "IsDeleted",
                "Orders");

            migrationBuilder.DropColumn(
                "IsDeleted",
                "Order Details");

            migrationBuilder.DropColumn(
                "IsDeleted",
                "Customers");

            migrationBuilder.DropColumn(
                "IsDeleted",
                "Categories");
        }
    }
}