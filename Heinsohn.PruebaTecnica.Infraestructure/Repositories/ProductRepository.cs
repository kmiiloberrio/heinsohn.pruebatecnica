﻿using Heinsohn.PruebaTecnica.Domain.Entities;
using Heinsohn.PruebaTecnica.Domain.IRepositories;
using Heinsohn.PruebaTecnica.Infraestructure.Context;

namespace Heinsohn.PruebaTecnica.Infraestructure.Repositories
{
    public class ProductRepository : GenericContextRepository<Product>, IProductRepository
    {
        public ProductRepository(NorthwindDbContext context) : base(context)
        {
        }
    }
}