﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Heinsohn.PruebaTecnica.Domain.IRepositories;
using Heinsohn.PruebaTecnica.Infraestructure.Context;
using Microsoft.EntityFrameworkCore;

namespace Heinsohn.PruebaTecnica.Infraestructure.Repositories
{
    public class GenericContextRepository<T> : IGenericContextRepository<T> where T : class, new()
    {
        private readonly NorthwindDbContext _context;

        protected GenericContextRepository(NorthwindDbContext context)
        {
            _context = context;
        }

        public T GetById(int id)
        {
            return _context.Set<T>().Find(id);
        }

        public IEnumerable<T> GetAll()
        {
            return _context.Set<T>();
        }

        public IEnumerable<T> Find(Expression<Func<T, bool>> expression)
        {
            return _context.Set<T>().Where(expression);
        }

        public IEnumerable<T> FindAll(params Expression<Func<T, object>>[] includes)
        {
            IQueryable<T> query = _context.Set<T>();
            foreach (var include in includes) query = query.Include(include);

            return query.ToList();
        }

        public void Add(T entity)
        {
            _context.Set<T>().Add(entity);
            _context.SaveChanges();
        }

        public void AddRange(IEnumerable<T> entities)
        {
            _context.Set<T>().AddRange(entities);
            _context.SaveChanges();
        }

        public void Update(T entity)
        {
            if (entity == null) throw new ArgumentNullException($"{nameof(Update)} entity must not be null");

            try
            {
                _context.Update(entity);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception($"{nameof(entity)} could not be updated: {ex.Message}");
            }
        }

        public void Remove(T entity)
        {
            _context.Set<T>().Remove(entity);
            _context.SaveChanges();
        }

        public void RemoveRange(IEnumerable<T> entities)
        {
            _context.Set<T>().RemoveRange(entities);
            _context.SaveChanges();
        }
    }
}