﻿using System;
using System.Collections.Generic;
using System.Linq;
using Heinsohn.PruebaTecnica.Domain.IRepositories;

namespace Heinsohn.PruebaTecnica.Infraestructure.Repositories
{
    public class GenericInMemRepository<T> : IGenericInMemRepository<T> where T : class
    {
        private readonly List<T> _items;

        protected GenericInMemRepository(List<T> items)
        {
            _items = items;
        }

        public T GetById(Func<T, bool> predicate)
        {
            return _items.SingleOrDefault(predicate);
        }

        public IEnumerable<T> GetAll()
        {
            return _items;
        }

        public IEnumerable<T> Find(Func<T, bool> predicate)
        {
            return _items.Where(predicate);
        }

        public void Add(T entity)
        {
            _items.Add(entity);
        }

        public void AddRange(IEnumerable<T> entities)
        {
            _items.AddRange(entities);
        }

        public void Update(Predicate<T> predicate, T entity)
        {
            var index = _items.FindIndex(predicate);
            _items[index] = entity;
        }

        public void Remove(Predicate<T> predicate)
        {
            var index = _items.FindIndex(predicate);
            _items.RemoveAt(index);
        }

        public void RemoveRange(IEnumerable<T> entities)
        {
            throw new NotImplementedException();
        }
    }
}