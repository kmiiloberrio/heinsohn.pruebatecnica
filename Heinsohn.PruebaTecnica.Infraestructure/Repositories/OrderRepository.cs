﻿using Heinsohn.PruebaTecnica.Domain.Entities;
using Heinsohn.PruebaTecnica.Domain.IRepositories;
using Heinsohn.PruebaTecnica.Infraestructure.Context;

namespace Heinsohn.PruebaTecnica.Infraestructure.Repositories
{
    public class OrderRepository : GenericContextRepository<Order>, IOrderRepository
    {
        public OrderRepository(NorthwindDbContext context) : base(context)
        {
        }
    }
}