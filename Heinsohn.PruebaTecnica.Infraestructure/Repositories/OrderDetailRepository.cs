﻿using Heinsohn.PruebaTecnica.Domain.Entities;
using Heinsohn.PruebaTecnica.Domain.IRepositories;
using Heinsohn.PruebaTecnica.Infraestructure.Context;

namespace Heinsohn.PruebaTecnica.Infraestructure.Repositories
{
    public class OrderDetailRepository : GenericContextRepository<OrderDetail>, IOrderDetailRepository
    {
        private readonly NorthwindDbContext _context;

        public OrderDetailRepository(NorthwindDbContext context) : base(context)
        {
            _context = context;
        }

        public OrderDetail GetById(int orderId, int productId)
        {
            return _context.Set<OrderDetail>().Find(orderId, productId);
        }
    }
}