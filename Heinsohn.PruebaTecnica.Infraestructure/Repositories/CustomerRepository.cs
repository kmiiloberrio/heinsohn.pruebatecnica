﻿using Heinsohn.PruebaTecnica.Domain.Entities;
using Heinsohn.PruebaTecnica.Domain.IRepositories;
using Heinsohn.PruebaTecnica.Infraestructure.Context;

namespace Heinsohn.PruebaTecnica.Infraestructure.Repositories
{
    public class CustomerRepository : GenericContextRepository<Customer>, ICustomerRepository
    {
        private readonly NorthwindDbContext _northwindDbContext;

        public CustomerRepository(NorthwindDbContext northwindDbContext) : base(northwindDbContext)
        {
            _northwindDbContext = northwindDbContext;
        }

        public Customer GetById(string id)
        {
            return _northwindDbContext.Set<Customer>().Find(id);
        }
    }
}