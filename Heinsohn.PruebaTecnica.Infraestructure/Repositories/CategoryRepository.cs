﻿using Heinsohn.PruebaTecnica.Domain.Entities;
using Heinsohn.PruebaTecnica.Domain.IRepositories;
using Heinsohn.PruebaTecnica.Infraestructure.Context;

namespace Heinsohn.PruebaTecnica.Infraestructure.Repositories
{
    public class CategoryRepository : GenericContextRepository<Category>, ICategoryRepository
    {
        public CategoryRepository(NorthwindDbContext context) : base(context)
        {
        }
    }
}