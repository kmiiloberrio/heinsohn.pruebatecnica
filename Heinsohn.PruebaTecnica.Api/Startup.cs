using Heinsohn.PruebaTecnica.Infraestructure;
using Heinsohn.PruebaTecnica.Infraestructure.Context;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;

namespace Heinsohn.PruebaTecnica.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.

        public void ConfigureServices(IServiceCollection services)
        {
            RegisterApplicationServices(services);
            RegisterRepository(services);

            //Cors
            services.AddCors(options =>
            {
                options.AddPolicy("DevCorsPolicy",
                    builder => builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod());
            });

            //AddContext
            services.AddDbContext<NorthwindDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("NorthwindDatabase")));

            //API
            services.AddControllers().AddNewtonsoftJson();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo {Title = "Heinsohn.PruebaTecnica.Api", Version = "v1"});
            });
        }

        private void RegisterApplicationServices(IServiceCollection services)
        {
            DependencyInjection.RegisterApplicationServices(services);
        }

        private void RegisterRepository(IServiceCollection services)
        {
            DependencyInjection.RegisterRepository(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, NorthwindDbContext context)
        {
            if (env.IsDevelopment())
            {
                app.UseCors("DevCorsPolicy");

                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Heinsohn.PruebaTecnica.Api v1"));
            }

            context.Database.EnsureCreated();
            context.ChangeTracker.LazyLoadingEnabled = false;
            context.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}