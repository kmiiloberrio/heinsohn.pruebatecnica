﻿using System;
using System.Collections.Generic;
using Heinsohn.PruebaTecnica.Application.Dtos;
using Heinsohn.PruebaTecnica.Application.IServices;
using Microsoft.AspNetCore.Mvc;

namespace Heinsohn.PruebaTecnica.Api.Controllers
{
    [ApiController]
    [Route("products")]
    public class ProductController : ControllerBase
    {
        private readonly IProductService _productService;

        public ProductController(IProductService productService)
        {
            _productService = productService;
        }

        // GET /products
        [HttpGet]
        public IEnumerable<ProductDto> GetProducts()
        {
            var products = _productService.GetProducts();
            return products;
        }

        // GET /products/{id}
        [HttpGet("{id}")]
        public ActionResult<ProductDto> GetProduct(int id)
        {
            var product = _productService.GetProduct(id);

            if (product is null) return NotFound();

            return product;
        }

        // POST /products
        [HttpPost]
        public ActionResult<ProductDto> CreateProduct(CreateProductDto creationProductDto)
        {
            var product = _productService.CreateProduct(creationProductDto);

            return CreatedAtAction(nameof(CreateProduct), new {id = product.ProductId}, product);
        }

        // PUT /products/{id}
        [HttpPut("{id}")]
        public ActionResult UpdateProduct(int id, ProductDto productDto)
        {
            var existingProduct = _productService.GetProduct(id);

            if (existingProduct is null) return NotFound();

            productDto.ProductId = existingProduct.ProductId;
            _productService.UpdateProduct(productDto);

            return NoContent();
        }

        // DELETE /products/{id}
        [HttpDelete("{id}")]
        public ActionResult DeleteProduct(int id)
        {
            try
            {
                if (!ModelState.IsValid) return BadRequest(ModelState);


                var item = _productService.GetProduct(id);

                if (item == null) return BadRequest();

                _productService.DeleteProduct(item);

                return new NoContentResult();
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return BadRequest(ModelState);
            }
        }
    }
}