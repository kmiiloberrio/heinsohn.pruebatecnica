﻿using System;
using System.Collections.Generic;
using System.Linq;
using Heinsohn.PruebaTecnica.Application.Dtos;
using Heinsohn.PruebaTecnica.Application.IServices;
using Microsoft.AspNetCore.Mvc;

namespace Heinsohn.PruebaTecnica.Api.Controllers
{
    [ApiController]
    [Route("orders")]
    public class OrderController : ControllerBase
    {
        private readonly IOrderService _orderService;

        public OrderController(IOrderService orderService)
        {
            _orderService = orderService;
        }

        // GET /orders
        [HttpGet]
        public IEnumerable<OrderDto> GetOrders()
        {
            var orders = _orderService.GetOrders();
            var filterOrder = orders
                .Where(dto => dto.OrderDate <= DateTimeOffset.Now && dto.CustomerId.Equals("ALFKI")).ToList();
            return filterOrder;
        }

        // GET /orders/{id}
        [HttpGet("{id}")]
        public ActionResult<OrderDto> GetOrder(int id)
        {
            var order = _orderService.GetOrder(id);

            if (order is null) return NotFound();

            return order;
        }

        // POST /orders
        [HttpPost]
        public ActionResult<OrderDto> CreateOrder(OrderDto orderDto)
        {
            try
            {
                if (!ModelState.IsValid) return BadRequest(ModelState);

                if (orderDto is null) return BadRequest();

                _orderService.CreateOrder(orderDto);

                var key = orderDto.OrderId;

                var itemToReturn = _orderService.GetOrder(key);

                return CreatedAtAction(nameof(CreateOrder), new {id = itemToReturn.OrderId}, itemToReturn);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return BadRequest(ModelState);
            }
        }

        // PUT /orders/{id}
        [HttpPut("{id}")]
        public ActionResult UpdateOrder(int id, OrderDto orderDto)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                if (orderDto == null || orderDto.OrderId != id)
                    return BadRequest();


                _orderService.UpdateOrder(orderDto);

                return new ObjectResult(orderDto);
                return NoContent();
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return BadRequest(ModelState);
            }
        }

        // DELETE /orders/{id}
        [HttpDelete("{id}")]
        public ActionResult DeleteOrder(int id)
        {
            try
            {
                if (!ModelState.IsValid) return BadRequest(ModelState);

                var existingOrder = _orderService.GetOrder(id);

                if (existingOrder is null) return BadRequest();

                _orderService.DeleteOrder(existingOrder);

                return NoContent();
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return BadRequest(ModelState);
            }
        }
    }
}