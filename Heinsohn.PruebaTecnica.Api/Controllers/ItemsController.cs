﻿using System;
using System.Collections.Generic;
using Heinsohn.PruebaTecnica.Application.Dtos;
using Heinsohn.PruebaTecnica.Application.IServices;
using Microsoft.AspNetCore.Mvc;

namespace Heinsohn.PruebaTecnica.Api.Controllers
{
    [ApiController]
    [Route("items")]
    public class ItemsController : ControllerBase
    {
        private readonly IItemService _itemService;

        public ItemsController(IItemService itemService)
        {
            _itemService = itemService;
        }

        // GET /items
        [HttpGet]
        public IEnumerable<ItemDto> GetItems()
        {
            var items = _itemService.GetItems();
            return items;
        }

        // GET /items/{id}
        [HttpGet("{id}")]
        public ActionResult<ItemDto> GetItem(Guid id)
        {
            var item = _itemService.GetItem(id);

            if (item is null) return NotFound();

            return item;
        }

        // POST /items
        [HttpPost]
        public ActionResult<ItemDto> CreateItem(CreateItemDto creationItemDto)
        {
            var item = _itemService.CreateItem(creationItemDto);

            return CreatedAtAction(nameof(CreateItem), new {id = item.Id}, item);
        }

        // PUT /items/{id}
        [HttpPut("{id}")]
        public ActionResult UpdateItem(Guid id, UpdateItemDto updateItemDto)
        {
            var existingItem = _itemService.GetItem(id);

            if (existingItem is null) return NotFound();

            var itemDto = existingItem with
            {
                Name = updateItemDto.Name,
                Price = updateItemDto.Price
            };

            _itemService.UpdateItem(itemDto);

            return NoContent();
        }

        // DELETE /items/{id}
        [HttpDelete("{id}")]
        public ActionResult DeleteItem(Guid id)
        {
            var existingItem = _itemService.GetItem(id);

            if (existingItem is null) return NotFound();

            _itemService.DeleteItem(id);

            return NoContent();
        }
    }
}