﻿using System.Collections.Generic;
using Heinsohn.PruebaTecnica.Application.Dtos;
using Heinsohn.PruebaTecnica.Application.IServices;
using Microsoft.AspNetCore.Mvc;

namespace Heinsohn.PruebaTecnica.Api.Controllers
{
    [ApiController]
    [Route("customers")]
    public class CustomerController : ControllerBase
    {
        private readonly ICustomerService _customerService;

        public CustomerController(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        // GET /customers
        [HttpGet]
        public IEnumerable<CustomerDto> GetCustomers()
        {
            var customer = _customerService.GetCustomers();
            return customer;
        }

        // GET /customers/{id}
        [HttpGet("{id}")]
        public ActionResult<CustomerDto> GetCustomer(string id)
        {
            var item = _customerService.GetCustomer(id);

            if (item is null) return NotFound();

            return item;
        }

        // POST /customers
        [HttpPost]
        public ActionResult<CustomerDto> CreateCustomer(CreateCustomerDto createCustomerDto)
        {
            var item = _customerService.CreateCustomer(createCustomerDto);

            return CreatedAtAction(nameof(CreateCustomer), new {id = item.CustomerId}, item);
        }

        // PUT /customers/{id}
        [HttpPut("{id}")]
        public ActionResult UpdateCustomer(string id, CustomerDto customerDto)
        {
            if (!ModelState.IsValid) return BadRequest();
            if (customerDto is null || id != customerDto.CustomerId) return NotFound();
            _customerService.UpdateCustomer(customerDto);

            return NoContent();
        }

        // DELETE /customers/{id}
        [HttpDelete("{id}")]
        public ActionResult DeleteCustomer(string id)
        {
            var existingItem = _customerService.GetCustomer(id);

            if (existingItem is null) return NotFound();

            _customerService.DeleteCustomer(existingItem);

            return NoContent();
        }
    }
}