﻿using System.Collections.Generic;
using Heinsohn.PruebaTecnica.Application.Dtos;
using Heinsohn.PruebaTecnica.Application.IServices;
using Microsoft.AspNetCore.Mvc;

namespace Heinsohn.PruebaTecnica.Api.Controllers
{
    [ApiController]
    [Route("categories")]
    public class CategoryController : ControllerBase
    {
        private readonly ICategoryService _categoryService;

        public CategoryController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        // GET /categories
        [HttpGet]
        public IEnumerable<CategoryDto> GetCategories()
        {
            var categories = _categoryService.GetCategories();
            return categories;
        }

        // GET /categories/{id}
        [HttpGet("{id}")]
        public ActionResult<CategoryDto> GetCategory(int id)
        {
            var item = _categoryService.GetCategory(id);

            if (item is null) return NotFound();

            return item;
        }

        // POST /categories
        [HttpPost]
        public ActionResult<CategoryDto> CreateCategory(CreateCategoryDto createCategoryDto)
        {
            var category = _categoryService.CreateCategory(createCategoryDto);

            return CreatedAtAction(nameof(CreateCategory), new {id = category.CategoryId}, category);
        }

        // PUT /categories/{id}
        [HttpPut("{id}")]
        public ActionResult UpdateCategory(int id, CategoryDto categoryDto)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);


            if (categoryDto is null || id != categoryDto.CategoryId) return BadRequest();

            _categoryService.UpdateCategory(categoryDto);

            return NoContent();
        }

        // DELETE /categories/{id}
        [HttpDelete("{id}")]
        public ActionResult DeleteCategory(int id)
        {
            if (!ModelState.IsValid) return BadRequest(ModelState);

            var existingItem = _categoryService.GetCategory(id);

            if (existingItem is null) return NotFound();

            _categoryService.DeleteCategory(existingItem);

            return NoContent();
        }
    }
}