﻿using System;
using System.Collections.Generic;
using Heinsohn.PruebaTecnica.Application.Dtos;
using Heinsohn.PruebaTecnica.Application.IServices;
using Microsoft.AspNetCore.Mvc;

namespace Heinsohn.PruebaTecnica.Api.Controllers
{
    [ApiController]
    [Route("order-details")]
    public class OrderDetailController : ControllerBase
    {
        private readonly IOrderDetailService _orderDetailService;

        public OrderDetailController(IOrderDetailService orderDetailService)
        {
            _orderDetailService = orderDetailService;
        }


        // GET /order-details
        [HttpGet]
        public IEnumerable<OrderDetailDto> GetOrderDetails()
        {
            var items = _orderDetailService.GetOrderDetails();
            return items;
        }


        // GET /order-details/{OrderI},{productId}
        [HttpGet("{orderId},{productId}")]
        public ActionResult<OrderDetailDto> GetOrderDetail(int orderId, int productId)
        {
            var item = _orderDetailService.GetOrderDetail(orderId, productId);
            return item;
        }


        // DELETE /order-details/{orderId},{productId}
        [HttpDelete("{orderId},{productId}")]
        public ActionResult DeleteOrderDetail(int orderId, int productId)
        {
            try
            {
                if (!ModelState.IsValid) return BadRequest(ModelState);


                var item = _orderDetailService.GetOrderDetail(orderId, productId);

                if (item is null) return BadRequest();


                _orderDetailService.DeleteOrderDetail(item);

                return NoContent();
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return BadRequest(ModelState);
            }
        }

        // PUT /order-details/{orderId},{productId}
        [HttpPut("{orderId},{productId}")]
        public ActionResult UpdateOrderDetail([FromRoute] int orderId, [FromRoute] int productId,
            [FromBody] OrderDetailDto orderDetailDto)
        {
            try
            {
                if (!ModelState.IsValid) return BadRequest(ModelState);

                if (orderDetailDto is null ||
                    orderDetailDto.OrderId != orderId && orderDetailDto.ProductId != productId)
                    return BadRequest();

                _orderDetailService.UpdateOrderDetail(orderDetailDto);

                return new ObjectResult(orderDetailDto);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return BadRequest(ModelState);
            }
        }

        // POST /order-details/{OrderDetail}
        [HttpPost]
        public ActionResult CreateOrderDetail(OrderDetailDto orderDetailDto)
        {
            try
            {
                if (!ModelState.IsValid) return BadRequest(ModelState);

                if (orderDetailDto is null) return BadRequest();


                _orderDetailService.CreateOrderDetail(orderDetailDto);

                var orderId = orderDetailDto.OrderId;
                var productId = orderDetailDto.ProductId;

                var itemToReturn = _orderDetailService.GetOrderDetail(orderId, productId);


                return new ObjectResult(itemToReturn)
                {
                    StatusCode = 201
                };
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return BadRequest(ModelState);
            }
        }
    }
}