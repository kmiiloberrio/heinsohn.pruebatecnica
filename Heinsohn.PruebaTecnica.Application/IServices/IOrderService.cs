﻿using System.Collections.Generic;
using Heinsohn.PruebaTecnica.Application.Dtos;

namespace Heinsohn.PruebaTecnica.Application.IServices
{
    public interface IOrderService
    {
        IEnumerable<OrderDto> GetOrders();
        OrderDto GetOrder(int id);
        void CreateOrder(OrderDto orderDto);
        void UpdateOrder(OrderDto orderDto);
        void DeleteOrder(OrderDto id);
    }
}