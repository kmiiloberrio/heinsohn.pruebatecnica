﻿using System.Collections.Generic;
using Heinsohn.PruebaTecnica.Application.Dtos;

namespace Heinsohn.PruebaTecnica.Application.IServices
{
    public interface ICategoryService
    {
        IEnumerable<CategoryDto> GetCategories();
        CategoryDto GetCategory(int id);
        CategoryDto CreateCategory(CreateCategoryDto createCategoryDto);
        void UpdateCategory(CategoryDto categoryDto);
        void DeleteCategory(CategoryDto categoryDto);
    }
}