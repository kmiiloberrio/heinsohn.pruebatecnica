﻿using System.Collections.Generic;
using Heinsohn.PruebaTecnica.Application.Dtos;

namespace Heinsohn.PruebaTecnica.Application.IServices
{
    public interface ICustomerService
    {
        IEnumerable<CustomerDto> GetCustomers();
        CustomerDto GetCustomer(string id);
        CustomerDto CreateCustomer(CreateCustomerDto createCustomerDto);
        void UpdateCustomer(CustomerDto customerDto);
        void DeleteCustomer(CustomerDto customerDto);
    }
}