﻿using System.Collections.Generic;
using Heinsohn.PruebaTecnica.Application.Dtos;

namespace Heinsohn.PruebaTecnica.Application.IServices
{
    public interface IProductService
    {
        IEnumerable<ProductDto> GetProducts();
        ProductDto GetProduct(int id);
        ProductDto CreateProduct(CreateProductDto itemDto);
        void UpdateProduct(ProductDto itemDto);
        void DeleteProduct(ProductDto id);
    }
}