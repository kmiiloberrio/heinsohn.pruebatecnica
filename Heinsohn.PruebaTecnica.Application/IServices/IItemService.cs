﻿using System;
using System.Collections.Generic;
using Heinsohn.PruebaTecnica.Application.Dtos;

namespace Heinsohn.PruebaTecnica.Application.IServices
{
    public interface IItemService
    {
        IEnumerable<ItemDto> GetItems();
        ItemDto GetItem(Guid id);
        ItemDto CreateItem(CreateItemDto itemDto);
        void UpdateItem(ItemDto itemDto);
        void DeleteItem(Guid id);
    }
}