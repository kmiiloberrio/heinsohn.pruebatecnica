﻿using System.Collections.Generic;
using Heinsohn.PruebaTecnica.Application.Dtos;

namespace Heinsohn.PruebaTecnica.Application.IServices
{
    public interface IOrderDetailService
    {
        IEnumerable<OrderDetailDto> GetOrderDetails();
        OrderDetailDto GetOrderDetail(int orderId, int productId);
        OrderDetailDto CreateOrderDetail(OrderDetailDto orderDetailDto);
        void UpdateOrderDetail(OrderDetailDto orderDetailDto);
        void DeleteOrderDetail(OrderDetailDto orderDetailDto);
    }
}