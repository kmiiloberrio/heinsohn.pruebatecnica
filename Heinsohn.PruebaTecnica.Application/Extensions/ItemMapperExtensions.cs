﻿using Heinsohn.PruebaTecnica.Application.Dtos;
using Heinsohn.PruebaTecnica.Domain.Entities;

namespace Heinsohn.PruebaTecnica.Application.Extensions
{
    public static class ItemMapperExtensions
    {
        public static ItemDto AsDto(this Item item)
        {
            return new()
            {
                Id = item.Id,
                Name = item.Name,
                Price = item.Price,
                CreatedDate = item.CreatedDate
            };
        }
    }
}