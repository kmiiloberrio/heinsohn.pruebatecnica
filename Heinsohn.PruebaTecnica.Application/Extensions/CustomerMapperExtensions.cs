﻿using Heinsohn.PruebaTecnica.Application.Dtos;
using Heinsohn.PruebaTecnica.Domain.Entities;

namespace Heinsohn.PruebaTecnica.Application.Extensions
{
    public static class CustomerMapperExtensions
    {
        public static CustomerDto AsDto(this Customer customer)
        {
            return new()
            {
                CustomerId = customer.CustomerId,
                CompanyName = customer.CompanyName,
                ContactName = customer.ContactName,
                ContactTitle = customer.ContactTitle,
                Address = customer.Address,
                City = customer.City,
                Region = customer.Region,
                PostalCode = customer.PostalCode,
                Country = customer.Country,
                Phone = customer.Phone,
                Fax = customer.Fax
            };
        }

        public static Customer AsEntity(this CustomerDto customerDto)
        {
            return new()
            {
                CustomerId = customerDto.CustomerId,
                CompanyName = customerDto.CompanyName,
                ContactName = customerDto.ContactName,
                ContactTitle = customerDto.ContactTitle,
                Address = customerDto.Address,
                City = customerDto.City,
                Region = customerDto.Region,
                PostalCode = customerDto.PostalCode,
                Country = customerDto.Country,
                Phone = customerDto.Phone,
                Fax = customerDto.Fax
            };
        }
    }
}