﻿using System.Collections.Generic;
using System.Linq;
using Heinsohn.PruebaTecnica.Application.Dtos;
using Heinsohn.PruebaTecnica.Domain.Entities;

namespace Heinsohn.PruebaTecnica.Application.Extensions
{
    public static class OrderDetailMapperExtensions
    {
        public static OrderDetailDto AsDto(this OrderDetail orderDetail)
        {
            return new()
            {
                OrderId = orderDetail.OrderId,
                ProductId = orderDetail.ProductId,
                UnitPrice = orderDetail.UnitPrice,
                Quantity = orderDetail.Quantity,
                Order = orderDetail.Order?.AsDto(),
                Product = orderDetail.Product?.AsDto()
            };
        }

        public static ICollection<OrderDetailDto> AsDto(this ICollection<OrderDetail> orderDetails)
        {
            var orderDetailDtos = orderDetails.Select(o => o.AsDto()).ToList();
            return orderDetailDtos;
        }

        public static OrderDetail AsEntity(this OrderDetailDto orderDetailDto)
        {
            return new()
            {
                OrderId = orderDetailDto.OrderId,
                ProductId = orderDetailDto.ProductId,
                UnitPrice = orderDetailDto.UnitPrice,
                Quantity = orderDetailDto.Quantity
            };
        }

        public static ICollection<OrderDetail> AsEntity(this ICollection<OrderDetailDto> orderDetailsDtos)
        {
            var orderDetails = orderDetailsDtos.Select(o => o.AsEntity()).ToList();
            return orderDetails;
        }
    }
}