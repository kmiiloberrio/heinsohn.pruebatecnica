﻿using Heinsohn.PruebaTecnica.Application.Dtos;
using Heinsohn.PruebaTecnica.Domain.Entities;

namespace Heinsohn.PruebaTecnica.Application.Extensions
{
    public static class CategoryMapperExtensions
    {
        public static CategoryDto AsDto(this Category item)
        {
            return new()
            {
                CategoryId = item.CategoryId,
                CategoryName = item.CategoryName,
                Description = item.Description,
                Products = item.Products?.AsDto()
            };
        }

        public static Category AsEntity(this CategoryDto item)
        {
            return new()
            {
                CategoryId = item.CategoryId,
                CategoryName = item.CategoryName,
                Description = item.Description
            };
        }
    }
}