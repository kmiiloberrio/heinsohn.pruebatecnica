﻿using System.Collections.Generic;
using System.Linq;
using Heinsohn.PruebaTecnica.Application.Dtos;
using Heinsohn.PruebaTecnica.Domain.Entities;

namespace Heinsohn.PruebaTecnica.Application.Extensions
{
    public static class ProductMapperExtensions
    {
        public static ProductDto AsDto(this Product item)
        {
            return new()
            {
                ProductId = item.ProductId,
                ProductName = item.ProductName,
                CategoryId = item.CategoryId,
                QuantityPerUnit = item.QuantityPerUnit,
                UnitPrice = item.UnitPrice,
                UnitsInStock = item.UnitsInStock,
                UnitsOnOrder = item.UnitsOnOrder,
                ReorderLevel = item.ReorderLevel,
                Discontinued = item.Discontinued
            };
        }

        public static ICollection<ProductDto> AsDto(this ICollection<Product> products)
        {
            var productDtos = products.Select(p => AsDto(p));
            return productDtos.ToList();
        }

        public static Product AsEntity(this ProductDto productDto)
        {
            return new()
            {
                ProductId = productDto.ProductId,
                ProductName = productDto.ProductName,
                CategoryId = productDto.CategoryId,
                QuantityPerUnit = productDto.QuantityPerUnit,
                UnitPrice = productDto.UnitPrice,
                UnitsInStock = productDto.UnitsInStock,
                UnitsOnOrder = productDto.UnitsOnOrder,
                ReorderLevel = productDto.ReorderLevel,
                Discontinued = productDto.Discontinued
            };
        }

        public static ICollection<Product> AsEntity(this ICollection<ProductDto> productDtos)
        {
            var products = productDtos.Select(p => p.AsEntity());
            return products.ToList();
        }
    }
}