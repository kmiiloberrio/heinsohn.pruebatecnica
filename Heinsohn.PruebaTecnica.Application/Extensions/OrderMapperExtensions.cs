﻿using System.Collections.Generic;
using System.Linq;
using Heinsohn.PruebaTecnica.Application.Dtos;
using Heinsohn.PruebaTecnica.Domain.Entities;

namespace Heinsohn.PruebaTecnica.Application.Extensions
{
    public static class OrderMapperExtensions
    {
        public static OrderDto AsDto(this Order order)
        {
            return new()
            {
                OrderId = order.OrderId,
                CustomerId = order.CustomerId,
                OrderDate = order.OrderDate,
                RequiredDate = order.RequiredDate
            };
        }

        public static ICollection<OrderDto> AsDto(this ICollection<Order> orders)
        {
            var ordersDto = orders.Select(p => p.AsDto()).ToList();
            return ordersDto;
        }

        public static Order AsEntity(this OrderDto orderDto)
        {
            return new()
            {
                OrderId = orderDto.OrderId,
                CustomerId = orderDto.CustomerId,
                OrderDate = orderDto.OrderDate,
                RequiredDate = orderDto.RequiredDate
            };
        }

        public static ICollection<Order> AsEntity(this ICollection<OrderDto> orderDtos)
        {
            var orders = orderDtos.Select(o => o.AsEntity());
            return orders.ToList();
        }
    }
}