﻿using System.Collections.Generic;
using System.Linq;
using Heinsohn.PruebaTecnica.Application.Dtos;
using Heinsohn.PruebaTecnica.Application.Extensions;
using Heinsohn.PruebaTecnica.Application.IServices;
using Heinsohn.PruebaTecnica.Domain.Entities;
using Heinsohn.PruebaTecnica.Domain.IRepositories;

namespace Heinsohn.PruebaTecnica.Application.Services
{
    public class ProductService : IProductService
    {
        private readonly IOrderDetailRepository _orderDetailsRepository;
        private readonly IProductRepository _productRepository;

        public ProductService(IProductRepository productRepository, IOrderDetailRepository orderDetailsRepository)
        {
            _productRepository = productRepository;
            _orderDetailsRepository = orderDetailsRepository;
        }

        public IEnumerable<ProductDto> GetProducts()
        {
            return _productRepository.GetAll().Select(c => c?.AsDto());
        }

        public ProductDto GetProduct(int id)
        {
            return _productRepository.GetById(id)?.AsDto();
        }

        public ProductDto CreateProduct(CreateProductDto createProductDto)
        {
            var order = new Product
            {
                ProductName = createProductDto.ProductName,
                CategoryId = createProductDto.CategoryId,
                QuantityPerUnit = createProductDto.QuantityPerUnit,
                UnitPrice = createProductDto.UnitPrice,
                UnitsInStock = createProductDto.UnitsInStock,
                UnitsOnOrder = createProductDto.UnitsOnOrder,
                ReorderLevel = createProductDto.ReorderLevel,
                Discontinued = createProductDto.Discontinued
            };
            _productRepository.Add(order);
            return order.AsDto();
        }

        public void UpdateProduct(ProductDto orderDto)
        {
            _productRepository.Update(orderDto.AsEntity());
        }

        public void DeleteProduct(ProductDto productDto)
        {
            var product = _productRepository.GetById(productDto.ProductId);
            _orderDetailsRepository.RemoveRange(_orderDetailsRepository.GetAll()
                .Where(orderDetail => orderDetail.ProductId == product.ProductId));
            _productRepository.Remove(product);
        }
    }
}