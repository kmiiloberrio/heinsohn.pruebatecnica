﻿using System.Collections.Generic;
using System.Linq;
using Heinsohn.PruebaTecnica.Application.Dtos;
using Heinsohn.PruebaTecnica.Application.Extensions;
using Heinsohn.PruebaTecnica.Application.IServices;
using Heinsohn.PruebaTecnica.Domain.Entities;
using Heinsohn.PruebaTecnica.Domain.IRepositories;

namespace Heinsohn.PruebaTecnica.Application.Services
{
    public class CustomerService : ICustomerService
    {
        private readonly ICustomerRepository _customerRepository;

        public CustomerService(ICustomerRepository customerRepository)
        {
            _customerRepository = customerRepository;
        }

        public IEnumerable<CustomerDto> GetCustomers()
        {
            return _customerRepository.GetAll().Select(c => c?.AsDto());
        }

        public CustomerDto GetCustomer(string id)
        {
            return _customerRepository.GetById(id)?.AsDto();
        }

        public CustomerDto CreateCustomer(CreateCustomerDto createCustomerDto)
        {
            var customer = new Customer
            {
                CustomerId = createCustomerDto.CustomerId,
                CompanyName = createCustomerDto.CompanyName,
                ContactName = createCustomerDto.ContactName,
                ContactTitle = createCustomerDto.ContactTitle,
                Address = createCustomerDto.Address,
                City = createCustomerDto.City,
                Region = createCustomerDto.Region,
                PostalCode = createCustomerDto.PostalCode,
                Country = createCustomerDto.Country,
                Phone = createCustomerDto.Phone,
                Fax = createCustomerDto.Fax
            };
            _customerRepository.Add(customer);
            return customer.AsDto();
        }

        public void UpdateCustomer(CustomerDto customerDto)
        {
            _customerRepository.Update(customerDto.AsEntity());
        }

        public void DeleteCustomer(CustomerDto customerDto)
        {
            _customerRepository.Remove(customerDto.AsEntity());
        }
    }
}