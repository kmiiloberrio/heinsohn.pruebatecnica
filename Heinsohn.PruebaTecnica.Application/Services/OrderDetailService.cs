﻿using System.Collections.Generic;
using System.Linq;
using Heinsohn.PruebaTecnica.Application.Dtos;
using Heinsohn.PruebaTecnica.Application.Extensions;
using Heinsohn.PruebaTecnica.Application.IServices;
using Heinsohn.PruebaTecnica.Domain.IRepositories;

namespace Heinsohn.PruebaTecnica.Application.Services
{
    public class OrderDetailService : IOrderDetailService
    {
        private readonly IOrderDetailRepository _orderDetailRepository;
        private readonly IProductRepository _productRepository;

        public OrderDetailService(IOrderDetailRepository orderDetailRepository, IProductRepository productRepository)
        {
            _orderDetailRepository = orderDetailRepository;
            _productRepository = productRepository;
        }


        public IEnumerable<OrderDetailDto> GetOrderDetails()
        {
            return _orderDetailRepository.FindAll(o => o.Product, o => o.Order)
                .Select(c => c?.AsDto());
        }

        public OrderDetailDto GetOrderDetail(int orderId, int productId)
        {
            return _orderDetailRepository.GetById(orderId, productId)?.AsDto();
        }

        public OrderDetailDto CreateOrderDetail(OrderDetailDto orderDetailDto)
        {
            _orderDetailRepository.Add(orderDetailDto.AsEntity());

            var product = _productRepository.GetById(orderDetailDto.ProductId);
            product.UnitsOnOrder += orderDetailDto.Quantity;
            _productRepository.Update(product);

            return orderDetailDto;
        }

        public void UpdateOrderDetail(OrderDetailDto orderDetailDto)
        {
            _orderDetailRepository.Update(orderDetailDto.AsEntity());
        }

        public void DeleteOrderDetail(OrderDetailDto orderDetailDto)
        {
            var orderDetail = _orderDetailRepository.GetById(orderDetailDto.OrderId, orderDetailDto.ProductId);
            _orderDetailRepository.Remove(orderDetail);
        }
    }
}