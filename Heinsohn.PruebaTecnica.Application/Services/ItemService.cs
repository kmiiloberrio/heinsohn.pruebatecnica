﻿using System;
using System.Collections.Generic;
using System.Linq;
using Heinsohn.PruebaTecnica.Application.Dtos;
using Heinsohn.PruebaTecnica.Application.Extensions;
using Heinsohn.PruebaTecnica.Application.IServices;
using Heinsohn.PruebaTecnica.Domain.Entities;
using Heinsohn.PruebaTecnica.Domain.IRepositories;

namespace Heinsohn.PruebaTecnica.Application.Services
{
    public class ItemService : IItemService
    {
        private readonly IInMemItemsRepository _repository;

        public ItemService(IInMemItemsRepository repository)
        {
            _repository = repository;
        }

        public IEnumerable<ItemDto> GetItems()
        {
            var items = _repository.GetAll().Select(item => item.AsDto());
            return items;
        }

        public ItemDto GetItem(Guid id)
        {
            return _repository.GetById(item => item.Id == id).AsDto();
        }

        public ItemDto CreateItem(CreateItemDto itemDto)
        {
            var item = new Item
            {
                Id = Guid.NewGuid(),
                Name = itemDto.Name,
                Price = itemDto.Price,
                CreatedDate = DateTimeOffset.Now
            };
            _repository.Add(item);

            return item.AsDto();
        }

        public void UpdateItem(ItemDto itemDto)
        {
            var item = new Item
            {
                Id = itemDto.Id,
                Name = itemDto.Name,
                Price = itemDto.Price,
                CreatedDate = itemDto.CreatedDate
            };
            _repository.Update(i => i.Id == item.Id, item);
        }

        public void DeleteItem(Guid id)
        {
            // _repository.DeleteItem(id);
            _repository.Remove(item => item.Id == id);
        }
    }
}