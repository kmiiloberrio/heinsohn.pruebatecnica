﻿using System.Collections.Generic;
using System.Linq;
using Heinsohn.PruebaTecnica.Application.Dtos;
using Heinsohn.PruebaTecnica.Application.Extensions;
using Heinsohn.PruebaTecnica.Application.IServices;
using Heinsohn.PruebaTecnica.Domain.Entities;
using Heinsohn.PruebaTecnica.Domain.IRepositories;

namespace Heinsohn.PruebaTecnica.Application.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly ICategoryRepository _categoryRepository;

        public CategoryService(ICategoryRepository categoryRepository)
        {
            _categoryRepository = categoryRepository;
        }

        public IEnumerable<CategoryDto> GetCategories()
        {
            return _categoryRepository.GetAll().Select(c => c?.AsDto());
        }

        public CategoryDto GetCategory(int id)
        {
            return _categoryRepository.GetById(id)?.AsDto();
        }

        public CategoryDto CreateCategory(CreateCategoryDto createCategoryDto)
        {
            var category = new Category
            {
                CategoryName = createCategoryDto.CategoryName,
                Description = createCategoryDto.Description
            };
            _categoryRepository.Add(category);
            return category.AsDto();
        }

        public void UpdateCategory(CategoryDto categoryDto)
        {
            _categoryRepository.Update(categoryDto.AsEntity());
        }

        public void DeleteCategory(CategoryDto categoryDto)
        {
            _categoryRepository.Remove(categoryDto.AsEntity());
        }
    }
}