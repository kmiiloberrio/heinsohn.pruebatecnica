﻿using System.Collections.Generic;
using System.Linq;
using Heinsohn.PruebaTecnica.Application.Dtos;
using Heinsohn.PruebaTecnica.Application.Extensions;
using Heinsohn.PruebaTecnica.Application.IServices;
using Heinsohn.PruebaTecnica.Domain.IRepositories;

namespace Heinsohn.PruebaTecnica.Application.Services
{
    public class OrderService : IOrderService
    {
        private readonly IOrderDetailRepository _orderDetailsRepository;
        private readonly IOrderRepository _orderRepository;
        private readonly IProductRepository _productRepository;

        public OrderService(IOrderRepository orderRepository, IOrderDetailRepository orderDetailsRepository,
            IProductRepository productRepository)
        {
            _orderRepository = orderRepository;
            _orderDetailsRepository = orderDetailsRepository;
            _productRepository = productRepository;
        }

        public IEnumerable<OrderDto> GetOrders()
        {
            return _orderRepository.GetAll().Select(c => c?.AsDto());
        }

        public OrderDto GetOrder(int id)
        {
            return _orderRepository.GetById(id)?.AsDto();
        }

        public void CreateOrder(OrderDto orderDto)
        {
            _orderRepository.Add(orderDto.AsEntity());
        }

        public void UpdateOrder(OrderDto orderDto)
        {
            _orderRepository.Update(orderDto.AsEntity());
        }

        public void DeleteOrder(OrderDto orderDto)
        {
            var orderDetails = _orderDetailsRepository.GetAll()
                .Where(orderDetail => orderDetail.OrderId == orderDto.OrderId);
            _orderDetailsRepository.RemoveRange(orderDetails);
            _orderRepository.Remove(orderDto.AsEntity());
        }
    }
}