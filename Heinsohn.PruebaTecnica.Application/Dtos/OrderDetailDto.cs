﻿namespace Heinsohn.PruebaTecnica.Application.Dtos
{
    public class OrderDetailDto
    {
        public int OrderId { get; set; }
        public int ProductId { get; set; }
        public decimal UnitPrice { get; set; }
        public short Quantity { get; set; }
        public OrderDto Order { get; set; }
        public ProductDto Product { get; set; }
    }
}