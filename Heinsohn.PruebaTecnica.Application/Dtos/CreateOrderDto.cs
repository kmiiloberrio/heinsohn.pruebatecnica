﻿using System;
using System.Collections.Generic;

namespace Heinsohn.PruebaTecnica.Application.Dtos
{
    public class CreateOrderDto
    {
        public string CustomerId { get; set; }
        public DateTime? OrderDate { get; set; }
        public DateTime? RequiredDate { get; set; }
        public ICollection<ProductOrderCreateDto> Products { get; set; }
    }
}