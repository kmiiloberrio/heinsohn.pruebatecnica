﻿namespace Heinsohn.PruebaTecnica.Application.Dtos
{
    public class CreateCategoryDto
    {
        public string CategoryName { get; set; }
        public string Description { get; set; }
    }
}