﻿using System.Collections.Generic;

namespace Heinsohn.PruebaTecnica.Application.Dtos
{
    public class CategoryDto
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string Description { get; set; }
        public ICollection<ProductDto> Products { get; set; }
    }
}