﻿using System;

namespace Heinsohn.PruebaTecnica.Application.Dtos
{
    public class OrderDto
    {
        public int OrderId { get; set; }
        public string CustomerId { get; set; }
        public DateTime? OrderDate { get; set; }
        public DateTime? RequiredDate { get; set; }
    }
}